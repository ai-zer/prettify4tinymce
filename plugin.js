/*
* Prettify syntax highlighter TinyMCE plugin
* Released under MIT License.
* 
* by Vusal Ganiev
* http://vusalstudio.ru
* https://bitbucket.org/ai-zer/prettify4tinymce
* 
*/

tinymce.PluginManager.requireLangPack('prettify4tinymce');
tinymce.PluginManager.add('prettify4tinymce', function (editor, url) {
    function showDialog() {
        /* Var declaration */
        var win, data = {}, preElem;
        var settings = {};

        /* settings : Default prettify code editor Form settings */
        settings.showLineNums = true;


        function onSubmitFunction(e) {
            var code = e.data.code;
            code = code.replace(/\</g, "&lt;").replace(/\>/g, "&gt;");
            var showLineNums = e.data.showLineNums ? 'linenums ' : '';
            var preClasses = e.data.additionalClasses;
            // Create pre element
            preElem = editor.dom.create('pre',
						{
						    class: 'prettyprint ' + showLineNums + preClasses
						},
                        code);
            
            editor.insertContent(editor.dom.getOuterHTML(preElem));
        }

        win = editor.windowManager.open({
            title: 'WinTitle',
            data: data,
            minWidth: 600,
            body: [
					{ name: 'code', type: 'textbox', minHeight: 200, multiline: true },
					{
					    type: 'container',
					    layout: 'flex',
					    direction: 'row',
					    align: 'center',
					    spacing: 7,
					    items: [
								{ name: 'showLineNums', type: 'checkbox', text: 'ShowLineNumbers', checked: settings.showLineNums }
					    ]
					},
					{
					    type: 'form',
					    padding: 0,
					    labelGap: 5,
					    spacing: 5,
					    direction: 'row',
					    items: [
								{ name: 'additionalClasses', type: 'textbox', label: 'AdditionalPreClasses', size: 50 }
					    ]
					}
            ],
            onsubmit: onSubmitFunction
        });
    }
    tinymce.DOM.loadCSS(url + '/style/style.css');

    editor.addButton('prettify4tinymce', {
        icon: 'prettify4tinymce',
        tooltip: 'IconHoverText',
        onclick: showDialog
    });
    editor.addMenuItem('prettify4tinymce', {
        text: 'IconHoverText',
        icon: 'prettify4tinymce',
        context: 'insert',
        onclick: showDialog
    });
});