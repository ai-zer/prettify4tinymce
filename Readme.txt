TinyMCE google-code-prettify  Plug-in

http://vusalstudio.ru
license    Licensed under the MIT license.
version    0.1
author     Vusal Ganiev
package    TinyMCE
name	   prettify4tinymce

Tested with: TinyMCE 4.3.3 

Installation
============
* Extract the zip to the TinyMCE plug-ins folder.
* Add prettify4tinymce to the plug-ins configuration.
* Add prettify4tinymce to the toolbar configuration.
