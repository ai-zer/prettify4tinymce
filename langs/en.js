tinymce.addI18n('en',{
    'WinTitle'              : 'Prettify4Tinymce Code Editor',
    'IconHoverText'         : 'Insert code',
    'ShowLineNumbers'       : 'Show line numbers',
    'AdditionalPreClasses'  : '"pre" additional classes'
});
